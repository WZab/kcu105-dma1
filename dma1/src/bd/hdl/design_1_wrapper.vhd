--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1.1 (lin64) Build 2580384 Sat Jun 29 08:04:45 MDT 2019
--Date        : Fri Mar  6 14:18:01 2020
--Host        : zbdaq1 running 64-bit Ubuntu 18.04.3 LTS
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    pcie_7x_mgt_rtl_rxn : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_7x_mgt_rtl_rxp : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_7x_mgt_rtl_txn : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_7x_mgt_rtl_txp : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_perstn : in STD_LOGIC;
    pcie_refclk_clk_n : in STD_LOGIC;
    pcie_refclk_clk_p : in STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    pcie_perstn : in STD_LOGIC;
    pcie_7x_mgt_rtl_rxn : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_7x_mgt_rtl_rxp : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_7x_mgt_rtl_txn : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_7x_mgt_rtl_txp : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_refclk_clk_p : in STD_LOGIC;
    pcie_refclk_clk_n : in STD_LOGIC
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      pcie_7x_mgt_rtl_rxn(7 downto 0) => pcie_7x_mgt_rtl_rxn(7 downto 0),
      pcie_7x_mgt_rtl_rxp(7 downto 0) => pcie_7x_mgt_rtl_rxp(7 downto 0),
      pcie_7x_mgt_rtl_txn(7 downto 0) => pcie_7x_mgt_rtl_txn(7 downto 0),
      pcie_7x_mgt_rtl_txp(7 downto 0) => pcie_7x_mgt_rtl_txp(7 downto 0),
      pcie_perstn => pcie_perstn,
      pcie_refclk_clk_n => pcie_refclk_clk_n,
      pcie_refclk_clk_p => pcie_refclk_clk_p
    );
end STRUCTURE;
